
function APIwhatever(aObject){
    
    let nameOfPort = document.getElementById("nameOfPort").value
    let countryOfPort = document.getElementById("countryOfPort").value
    let typeOfPort = document.getElementById("typeOfPort").value
    let sizeOfPort = document.getElementById("sizeOfPort").value
    if (nameOfPort === ''||countryOfPort === ''||typeOfPort === ''||sizeOfPort === ''){
        outputMessageRef.innerText = "Please fill in the empty boxes"
    }
    else{
        let lat = aObject.results[0].annotations.DMS.lat
        let lng = aObject.results[0].annotations.DMS.lng
        tempPort = new Port(nameOfPort, countryOfPort, typeOfPort, sizeOfPort, lat, lng)
        let portList = JSON.parse(localStorage.getItem("portList"))
        if(portList !== null){
            portList["Ports"].push(tempPort)
            jsonportListInstance = JSON.stringify(portList)
            localStorage.setItem("portList",jsonportListInstance)
        }
        else{
            portList = {
                Ports:[]
            }
            portList["Ports"].push(tempPort)
            jsonportListInstance = JSON.stringify(portList)
            localStorage.setItem("portList",jsonportListInstance)
        }
        window.open("index.html","_self")
    }
    
}
function addPort(){
    let outputMessageRef = document.getElementById("outputMessage")
    let locationOfPort = document.getElementById("locationOfPort").value
    if (locationOfPort === ''){
        outputMessageRef.innerText = "Please fill in the empty boxes"
    }
    else{
        let locationPort = locationOfPort.replace(' ','+')
        let url2 = 'https://api.opencagedata.com/geocode/v1/json?q=' + locationPort + '&key=921f9a8f732b438b96a7b76d8ba6a862' + '&callback=APIwhatever';
        let script = document.createElement('script');
        script.src = url2;
        document.body.appendChild(script);
    }
    
}
