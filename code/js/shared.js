//For testing purposes
//localStorage.removeItem("shipList")

//Create localstorage file for shiplist.
if (localStorage.getItem("shipList") === null) {
    let shipList = {ships: []}
    let jsonShipListInstance = JSON.stringify(shipList)
    localStorage.setItem("shipList",jsonShipListInstance)
} else {
    let shipList = JSON.parse(localStorage.getItem("shipList"))
}

class Ship
{
    constructor(name, maxSpeed, range, description, cost, status)
    {
        this._name = name
        this._maxSpeed = maxSpeed
        this._range = range
        this._description = description
        this._cost = cost
        this._ststus = status
    }
    
    get name()
    {
        return this._name
    }
    
    get maxSpeed()
    {
        this._maxSpeed
    }
    
    get range()
    {
        this._range
    }
    
    get description()
    {
        this._description
    }
    
    get cost()
    {
        this._cost
    }
    
    get status()
    {
        this._ststus
    }
}

class Route
    {
        constructor(name, ship, sourcePort, destinationPort, distance, time, cost, startDate, waypointList)
        {
            this._name = name;
            this._ship = ship;
            this._sourcePort = sourcePort;
            this._destinationPort = destinationPort;
            this._distance =  distance;
            this._time = time;
            this._cost = cost;
            this._startDate = startDate;
            this._waypointList=waypointList;
        }
         get name() 
    { 
        this._name
    } 
        
         get ship() 
    { 
        this._ship 
    } 
        
         get sourcePort() 
    { 
        this._sourcePort 
    } 
        
        get destinationPort() 
    { 
        this._destinationPort 
    } 
        
         get distance() 
    { 
        this._distance
    } 
        
         get time() 
    { 
        this._time 
    }
        
         get cost() 
    { 
        this._cost
    } 
        
         get starDate() 
    { 
        this._startDate
    }
        
         get waypointList() 
    { 
        this._waypointList 
    } 
        
    }

    class Port
{ 
    constructor(name, country, type, size, latitude, longtitude) 
    { 
        this._name = name 
        this._country = country 
        this._type = type 
        this._size = size 
        this._latitude = latitude 
        this._longtitude = longtitude 
    } 
     
    get name() 
    { 
        return this._name 
    } 
     
    get maxSpeed() 
    { 
        this._country 
    } 
     
    get range() 
    { 
        this._type 
    } 
     
    get description() 
    { 
        this._size 
    } 
     
    get cost() 
    { 
        this._latitude
    } 
     
    get status() 
    { 
        this._longtitude 
    } 
}