//Retrieve routes from local storage
let routeList = JSON.parse(localStorage.getItem("routeList"))

//Example route list
routeList = []
//name, ship, sourcePort, destinationPort, distance, time, cost, starDate, waypointList
//routeList.push(new Route("Test Route", "Honey Bee", "Source Port", "Destination Port", 2000, 20, 200, "2020-11-26"))
//routeList.push(new Route("Test Route", "Honey Bee", "Source Port", "Destination Port", 1000, 20, 200, "2020-11-25"))
//routeList.push(new Route("Test Route", "Honey Bee", "Source Port", "Destination Port", 3000, 20, 200, "2020-11-27"))

//ship API fetch ship max speed
let url = "https://eng1003.monash/api/v1/ships/?callback=shipAPI"
let script = document.createElement('script');
script.src = url;
document.body.appendChild(script);

let shipNameCostList = []
function shipAPI(shipList){
    for (let i = 0; i < shipList.ships.length ; i++ ){
        shipNameCostObject = {
            name: shipList.ships[i].name,
            maxSpeed: shipList.ships[i].maxSpeed
        }
        shipNameCostList.push(shipNameCostObject)
    }
}

//Run function after API loaded
window.onload = routeTable
function routeTable(){
    
    //output Div
    let viewRouteTableRef = document.getElementById("viewRouteTable")
    
    //error message for no route
    if (routeList.length === 0){
        let output = "<tr>"
        output += "<td colspan='5' style='text-align: center'>"
        output += "<b>There is currently no routes running</b><br>please create a route"
        output += "</td>"
        output += "</tr>"
        viewRouteTableRef.innerHTML = output
    } else {

        //Sort dates 
        let dateNotSorted = []
        let dateSorted = []
        for (let i = 0; i < routeList.length ; i++ ){
            dateNotSorted.push(new Date(routeList[i]._startDate))
        }
        function sortDate(a,b){ //The camparison function
            return a - b
        }
        dateNotSorted.sort(sortDate)
        for (let i = 0; i < dateNotSorted.length ; i++ ){
            dateSorted.push(dateNotSorted[i].toISOString().slice(0,10))
        }

        //For every date, 
        //Iterate over every route,
        //If route date = sorted date
        //Then, display that route first
        for (let i = 0; i < dateSorted.length ; i++ ){
            for (let j = 0; j < routeList.length ; j++ ){
                if (routeList[j]._startDate === dateSorted[i]){
                    let name = routeList[j]._name
                    let ship = routeList[j]._ship
                    let distance = routeList[j]._distance
                    let cost = routeList[j]._cost
                    let startDate = routeList[j]._startDate

                    //Finding ship speed
                    let speed;
                    for (let i = 0; i < shipNameCostList.length ; i++ ){
                        if (shipNameCostList[i].name === ship){
                            speed = shipNameCostList[i].maxSpeed
                            break
                        }
                    }

                    let totalCost = distance * cost
                    let toatalTime = distance / (speed * 1.852001)

                    //Displaying to HTML
                    
                    let output = ""
                    output += "<tr onclick = jumpToViewRoute(" + j + ") class='tableRowStyle0'>"
                    output += "<td class='mdl-data-table__cell--non-numeric'>" + name +  "</td>"
                    output += "<td>" + distance + "</td>"
                    output += "<td>" + totalCost + "</td>"
                    output += "<td>" + toatalTime.toFixed(2) + "</td>"
                    output += "<td>" + startDate + "</td>"
                    output +="</tr>"
                    viewRouteTableRef.innerHTML += output
                }
            }
        }
    }
}
function jumpToViewRoute(number){
    localStorage.setItem("theRoute",number)
    window.open("viewRoute.html","_self")
}