//Output to HTML
let viewShipsTableRef = document.getElementById("viewShipsTable")

//ship API
let url = "https://eng1003.monash/api/v1/ships/?callback=shipAPI"
let script = document.createElement('script');
script.src = url;
document.body.appendChild(script);

//retrieve and display ships from API
function shipAPI(shipList){
    
    //looping for every ship
    for (let i = 0; i < shipList.ships.length ; i++ ){
        
        //getting the data from each ship
        let name = shipList.ships[i].name
        let maxSpeed = shipList.ships[i].maxSpeed
        let range = shipList.ships[i].range
        let description = shipList.ships[i].desc
        let cost = shipList.ships[i].cost
        let status = shipList.ships[i].status
        let comments = shipList.ships[i].comments
        
        //Comments on orign of ship and general comments
        if (comments !== ""){
            let APIcomments = comments 
            comments = "Not User Generated, "
            comments += APIcomments
        } else {
            comments = "Not User Generated"
        }
        
        //table data row style 
        let tableColour = ""
        if(i % 2 == 0){
            tableColour = 1
        } else {
            tableColour = 0
        }
        
        //creating the HTML tags and displaying the ship data 
        let output = ""
        output += "<tr class='tableRowStyle0'>"
        output += "<td class='mdl-data-table__cell--non-numeric'>" + name +  "</td>"
        output += "<td>" + maxSpeed + "</td>"
        output += "<td>" + range + "</td>"
        output += "<td class='mdl-data-table__cell--non-numeric '>" + description + "</td>"
        output += "<td>" + cost + "</td>"
        output +="<td class='mdl-data-table__cell--non-numeric'>" + status + "</td>"
        output +="</tr>"
        output +="<tr class='tableRowStyle1'>"
        output +="<td colspan='5' style='text-align: left'>" + "Comments: " + comments + "<td>"
        output +="</tr>"
        viewShipsTableRef.innerHTML += output
    }
}


//retrieve data from local storage
let shipList = JSON.parse(localStorage.getItem("shipList"))

//looping for every ship
for (let i = 0; i < shipList.ships.length ; i++ ){
    //getting the data from each ship
    let name = shipList.ships[i]._name
    let maxSpeed = shipList.ships[i]._maxSpeed
    let range = shipList.ships[i]._range
    let description = shipList.ships[i]._description
    let cost = shipList.ships[i]._cost
    let status = shipList.ships[i]._status
    let comments = shipList.ships[i]._comments
    
    //Comments on orign of ship
    if (comments === "" || comments === undefined){
        comments = "User Generated Ship"
    }

    //creating the HTML and outputting the ship data
    //Style and colspan will NOT work if put in CSS
    let output = ""
    output += "<tr class='tableRowStyle0'>"
    output += "<td class='mdl-data-table__cell--non-numeric'>" + name +  "</td>"
    output += "<td>" + maxSpeed + "</td>"
    output += "<td>" + range + "</td>"
    output += "<td class='mdl-data-table__cell--non-numeric'>" + description + "</td>"
    output += "<td>" + cost + "</td>"
    output +="<td class='mdl-data-table__cell--non-numeric'>" + status + "</td>"
    output +="<tr class='tableRowStyle1'>"
    output +="<td  colspan='5' style='text-align: left'>" + "Comments: " + comments + "<td>"
    output +="</tr>"
    viewShipsTableRef.innerHTML += output
}