//ship API
let url = "https://eng1003.monash/api/v1/ships/?callback=shipAPI"
let script = document.createElement('script');
script.src = url;
document.body.appendChild(script);

//retrieve name of ships from API
let shipNames = []
function shipAPI(shipList){
    for (let i = 0; i < shipList.ships.length ; i++ ){
        shipNames.push(shipList.ships[i].name)
    }
    
    //Get ship nams from local storage
    let shipListLocal = JSON.parse(localStorage.getItem("shipList"))
    for (let i = 0; i < shipListLocal.ships.length ; i++ ){
        shipNames.push(shipListLocal.ships[i]._name)
    }
}

function addShip(){
    let nameOfShip = document.getElementById("nameOfShip").value
    let speedKnots = document.getElementById("speedKnots").value
    let range = document.getElementById("range").value
    let description = document.getElementById("description").value
    let cost = document.getElementById("cost").value
    
    //Information Validation
    let shipNameAvailable = 1
    for (let i = 0; i < shipNames.length ; i++ ){
        if (nameOfShip === shipNames[i]){
            shipNameAvailable = 0
            break
        }
    }
    
    let outputMessageRef = document.getElementById("outputMessage")
    if (nameOfShip === "" || speedKnots === "" || range === "" || description === "" || cost === ""){
        outputMessageRef.innerText = "Please fill in the empty boxes"
        
    } else if (isNaN(speedKnots) || isNaN(range) || isNaN(cost)) {
        outputMessageRef.innerText = "The input is not a number, please type in a number"
        
    } else if (shipNameAvailable === 0) {
        outputMessageRef.innerHTML = "The ship name has already has already been taken, please chose an <b>alternate name</b>"
        
    } else {
        //Opening index.HTML when ship is saved
        function openIndex(){
            window.open("index.html","_self")
        }
        //Output & Timeout To not startle users
        setTimeout(openIndex,4000)
        outputMessageRef.innerHTML = "The ship has been saved, you will be <b>redirected to the main page</b> shortly"
        
        let ship = new Ship(nameOfShip,speedKnots,range,description,cost,"Available")
        
        //Adding new ship into the shipList
        let shipList = JSON.parse(localStorage.getItem("shipList"))
        shipList["ships"].push(ship)
        let jsonShipListInstance = JSON.stringify(shipList)
        
        //Putting the new shipList into local storage
        localStorage.setItem("shipList",jsonShipListInstance)
    }
}