
function openMap(number){

    console.log(1)
    //show mapbox Div
    document.getElementById("mapDiv").style.display = "block"
    //creating mapbox
    let mapRef = document.getElementById("map")
    mapboxgl.accessToken = 'pk.eyJ1IjoicmljaHdvc29uIiwiYSI6ImNrY3llbnpkOTA5YjAydHFyeTFnYXZhcHoifQ.82zF5MXFpUG4Lwb9nc8GBQ'
    var map = new mapboxgl.Map({
    container: 'map', // container id
    style: 'mapbox://styles/mapbox/streets-v11', // style URL
    center: [13, 56], // starting position [lng, lat]
    zoom: 2 // starting zoom
    });
    routeList = JSON.parse(localStorage.getItem("routeList"))
    //marker Source Port (sp)
    let sourcelat = routeList.route[number]._sourcePort._latitude
    let sourcelng = routeList.route[number]._sourcePort._longtitude
    let spColor = {color: "#00ff00"}
    let spLangLat = [sourcelng, sourcelat]
    var marker = new mapboxgl.Marker(spColor)
        .setLngLat(spLangLat)
        .addTo(map);
    
    //marker Destination Port (dp)
    let destinationlat = routeList.route[number]._sourcePort._latitude
    let destinationlng = routeList.route[number]._sourcePort._longtitude
    let dpColor = {color: "#ff0000"}
    let dpLangLat = [destinationlng, destinationlat]
    var marker = new mapboxgl.Marker(dpColor)
        .setLngLat(dpLangLat)
        .addTo(map);
    
    //panning to center of sp and dp
    let spdpMiddle = [(spLangLat[0]+dpLangLat[0])/2, (spLangLat[1]+dpLangLat[1])/2]
    map.panTo(spdpMiddle, {duration: 5000});
    
    //Line between sp and dp
    let line = [spLangLat, dpLangLat]
    
    //diaplay line for the first time
    map.on('load', function () {
        map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': line
                }
            }
        });
        map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': '#888',
                'line-width': 8
            }
        });
    });
}
let theNumber = JSON.parse(localStorage.getItem("theRoute"))
openMap(theNumber)