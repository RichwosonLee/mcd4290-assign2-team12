let sourcePort = document.getElementById("sourcePort").value
let destinationPort = document.getElementById("destinationPort").value
let spLangLat = []
let dpLangLat = []
let line = []

//let output = document.getElementById("output")
function previewRoute(){
    //The ports API
    let url4 = 'https://eng1003.monash/api/v1/ports/?callback=checkPortAPI';
    let script = document.createElement('script');
    script.src = url4;
    document.body.appendChild(script)
}

//This functions is used to check if the user input name matches any names in the list
function checkPortAPI(portObject){
    
    let nameOfRoutes = document.getElementById("nameOfRoute").value
    let sourcePort = document.getElementById("sourcePort").value
    let destinationPort = document.getElementById("destinationPort").value
    let outputMessageRef = document.getElementById("outputMessagePreviewRoute")
    console.log(nameOfRoutes)
    //Route name validation
    
    
    sourceMatchCheck = false;
    destinationMatchCheck = false;
    
    //checking for name in the APIlist
    for(i = 0; i < portObject.ports.length; i++){
        if(portObject.ports[i].name === sourcePort){
            sourceMatchCheck = true
            spLangLat = [portObject.ports[i].lng, portObject.ports[i].lat]
        }
        if(portObject.ports[i].name === destinationPort){
            destinationMatchCheck = true
            dpLangLat = [portObject.ports[i].lng, portObject.ports[i].lat]
        }
        if((destinationMatchCheck === true)&&(sourceMatchCheck === true)){
            break
        }
    }
    
    //checking for the name in local storage
    portList = JSON.parse(localStorage.getItem("portList"))
    for(i = 0; i < portList.Ports.length; i++){
        if(portList.Ports[i]._name === sourcePort){
            sourceMatchCheck = true
            spLangLat = [portObject.ports[i].lng, portObject.ports[i]._latitude]
        }
        if(portList.Ports[i]._name === destinationPort){
            destinationMatchCheck = true
            spLangLat = [portObject.ports[i].lng, portObject.ports[i]._longtitude]
        }
        if((destinationMatchCheck === true)&&(sourceMatchCheck === true)){
            outputMessageRef.innerText = "Please add waypoints to the map as you wish"
            break
        }
    }
    //output the text information of the result
    if((destinationMatchCheck === false)&&(sourceMatchCheck === false)){
        outputMessageRef.innerText = "The destination port and source port you have enter have not found"
    }
    else if(sourceMatchCheck === false){
        outputMessageRef.innerText = "The source port you have enter have not found"
    }
    else if(destinationMatchCheck === false){
        outputMessageRef.innerText = "The destination port you have enter have not found"
    }
    else if(nameOfRoutes === ""){
        outputMessageRef.innerHTML = "Please input a <br>route name</br>"
    }
    else{
        outputMessageRef.innerHTML = "Your Ports have been found in our data base, Please <b>add waypoints</b> to the map as you wish"
        mapboxFunction()
    }
}
        

//Map box
// source port, destination port needed
//Distance calculator (https://www.geodatasource.com/developers/javascript)
function distance(lat1, lon1, lat2, lon2, unit) {
	if ((lat1 == lat2) && (lon1 == lon2)) {
		return 0;
	}
	else {
		var radlat1 = Math.PI * lat1/180;
		var radlat2 = Math.PI * lat2/180;
		var theta = lon1-lon2;
		var radtheta = Math.PI * theta/180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist);
		dist = dist * 180/Math.PI;
		dist = dist * 60 * 1.1515;
		if (unit=="K") { dist = dist * 1.609344 }
		if (unit=="N") { dist = dist * 0.8684 }
		return dist;
	}
}



function mapboxFunction(){
    
    //show mapbox Div
//    checkPort();
    document.getElementById("mapDiv").style.display = "block"
    //creating mapbox
    let mapRef = document.getElementById("map")
    mapboxgl.accessToken = 'pk.eyJ1IjoicmljaHdvc29uIiwiYSI6ImNrY3llbnpkOTA5YjAydHFyeTFnYXZhcHoifQ.82zF5MXFpUG4Lwb9nc8GBQ'
    var map = new mapboxgl.Map({
    container: 'map', // container id
    style: 'mapbox://styles/mapbox/streets-v11', // style URL
    center: [13, 56], // starting position [lng, lat]
    zoom: 2 // starting zoom
    });
    
    //marker Source Port (sp)
    let spColor = {color: "#00ff00"}
//    let spLangLat = [-0.2674, 50.83573]
    var marker = new mapboxgl.Marker(spColor)
        .setLngLat(spLangLat)
        .addTo(map);
    
    //marker Destination Port (dp)
    let dpColor = {color: "#ff0000"}
//    let dpLangLat = [-73.996, 40.71213]
    var marker = new mapboxgl.Marker(dpColor)
        .setLngLat(dpLangLat)
        .addTo(map);
    
    //panning to center of sp and dp
    let spdpMiddle = [(spLangLat[0]+dpLangLat[0])/2, (spLangLat[1]+dpLangLat[1])/2]
    map.panTo(spdpMiddle, {duration: 5000});
    
    //Line between sp and dp
    line = [spLangLat, dpLangLat]
    
    //diaplay line for the first time
    map.on('load', function () {
        map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': line
                }
            }
        });
        map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': '#888',
                'line-width': 8
            }
        });
        sortShip()
    });
    
    let wayPointAmount = 0
    
    map.on('click', function (e) {
        
        //get coordinates
        let newLat = e.lngLat.lat
        let newLng = e.lngLat.lng
        
        let minWaypointDistance = 100
        let wayPointDistance = minWaypointDistance + 1
        
        if (wayPointAmount !== 0){
            let lat1 = newLat
            let lon1 = newLng
            let lat2 = line[wayPointAmount][1]
            let lon2 = line[wayPointAmount][0]
            let unit = "K"
            wayPointDistance = distance(lat1, lon1, lat2, lon2, unit)
        }
        
        //vlidation check for waypoint (100 km)
        if (wayPointDistance <= minWaypointDistance){
            setTimeout(errMessage,4000)
            document.getElementById("message").innerHTML = "Distance between waypoints is too small, please try again"
            function errMessage(){
                document.getElementById("message").innerHTML = ""
            }
        } else {
            wayPointAmount ++
            //remove previous layer
            map.removeLayer('route')
            map.removeSource('route')
            
            //Put new waypoint in middle
            dpLangLat = line.pop()
            line.push([newLng, newLat])
            line.push(dpLangLat)

            //add new source and layer
                map.addSource('route', {
                'type': 'geojson',
                'data': {
                    'type': 'Feature',
                    'properties': {},
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': line
                        }
                    }
                });
            map.addLayer({
                'id': 'route',
                'type': 'line',
                'source': 'route',
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': '#888',
                    'line-width': 8
                }
            });
            
            sortShip()
            
        }
    });
}

window.onload = checkShip
function checkShip(){
    let url5 = "https://eng1003.monash/api/v1/ships/?callback=cheakShipAPI";
    let script = document.createElement('script');
    script.src = url5;
    document.body.appendChild(script)
}

//list of ship name and range
shipNameRange = []
function cheakShipAPI(shipObject){
    for (let i = 0; i < shipObject.ships.length ; i++ ){
        let name = shipObject.ships[i].name
        let range = shipObject.ships[i].range
        shipNameRange.push([name,range])
    }
    console.log(shipNameRange)//....................................................
}  
//local storage    
let shipList = JSON.parse(localStorage.getItem("shipList"))

//looping for every ship
for (let i = 0; i < shipList.ships.length ; i++ ){
        let name = shipList.ships[i]._name
        let range = shipList.ships[i]._range
        shipNameRange.push([name,range])
}

let distanceTravel = ""
function sortShip(){
    console.log("sort ship function ran")
    for (let i = 0; i < line.length-1 ; i++ ){
        let lon1 = line[i][0]
        let lat1 = line[i][1]
        let lon2 = line[i+1][0]
        let lat2 = line[i+1][1]
        distanceTravel += distance(lat1, lon1, lat2, lon2, "K")
    }
    for (let i = 0; i < shipNameRange.length ; i++ ){
        
    }
}

let ok = ""
function addRoute(){
    
    //Hide mapbox Div
    document.getElementById("mapDiv").style.display = "none"
    
    ok = document.getElementById("startDate").value
    console.log(ok)
    
    //name of ship display 
    let nameOfShip = document.getElementById("nameOfShip").value
    console.log(nameOfShip)
}