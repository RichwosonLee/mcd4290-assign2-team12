function viewPorts(){
    let url3 = 'https://eng1003.monash/api/v1/ports/?callback=insertAPI';
    let script = document.createElement('script');
    script.src = url3;
    document.body.appendChild(script);
}
function insertAPI(portObjects){
    let viewPortsTableRef = document.getElementById("viewPortsTable")
    
    for(i = 0;i < portObjects.ports.length;i++){
        tempScript = '<tr>';
        tempScript += '<th>' + portObjects.ports[i].name + '</th>'
        tempScript += '<th>' + portObjects.ports[i].country + '</th>'
        tempScript += '<th>' + portObjects.ports[i].type + '</th>'
        tempScript += '<th>' + portObjects.ports[i].size + '</th>'
        tempScript += '<th>' + portObjects.ports[i].locprecision + '</th>'
        tempScript += '<th>' + portObjects.ports[i].lat + '</th>'
        tempScript += '<th>' + portObjects.ports[i].lng + '</th>'
        tempScript += '</tr>'
        viewPortsTableRef.innerHTML += tempScript
    }
    tempScript2 = '<tr>User generated Ports</tr>'
    viewPortsTableRef.innerHTML += tempScript2
    portList = JSON.parse(localStorage.getItem("portList"))
    for(i = 0;i < portList.Ports.length;i++){
        tempScript = '<tr>';
        tempScript += '<th>' + portList.Ports[i]._name + '</th>'
        tempScript += '<th>' + portList.Ports[i]._country + '</th>'
        tempScript += '<th>' + portList.Ports[i]._type + '</th>'
        tempScript += '<th>' + portList.Ports[i]._size + '</th>'
        tempScript += '<th>' + 'unknown' + '</th>'
        tempScript += '<th>' + portList.Ports[i]._latitude + '</th>'
        tempScript += '<th>' + portList.Ports[i]._longtitude + '</th>'
        tempScript += '</tr>'
        viewPortsTableRef.innerHTML += tempScript
    }
}
viewPorts()